--
--
CREATE TABLE persons (
    id INTEGER,
    name VARCHAR(50)
)
;

CREATE TABLE person_to_company (
    person_id INTEGER,
    company_id INTEGER
)
;
