node {

    def projectName = "sebhoss/yosql"
    def repositoryLocation = ".repository"
    def githubCredentials = "build-metio-wtf-github"
    def BAZEL_VERSION = "0.4.5"
    def INSTALLER_PLATFORM = "linux-x86_64"
    def URL = "https://github.com/bazelbuild/bazel/releases/download/${BAZEL_VERSION}/bazel-${BAZEL_VERSION}-installer-${INSTALLER_PLATFORM}.sh"
    def BAZEL_INSTALLER = "bazel-installer.sh"
    def BASE = pwd() + "/bazel-install"

    stage('Checkout') {
        checkout scm
    }

    stage('Download Bazel Installer') {
        sh "curl -L -o ${BAZEL_INSTALLER} ${URL}"
    }

    stage('Install Bazel') {
        sh "chmod +x ${BAZEL_INSTALLER}"
        sh "rm -rf ${BASE}"
        sh "mkdir -p ${BASE}/bin"
        sh "mkdir -p ${BASE}/binary"
        sh "./${BAZEL_INSTALLER} --base=${BASE} --bazelrc=${BASE}/bin/bazel.bazelrc --bin=${BASE}/binary"
    }

    stage('Build Everything') {
        sh """
            ${BASE}/binary/bazel \
                --bazelrc=${BASE}/binary/bazel.bazelrc build ...
        """
    }

    stage('Test Everything') {
        sh """
            ${BASE}/binary/bazel \
                --bazelrc=${BASE}/binary/bazel.bazelrc test ...
        """
    }

    stage('Git config') {
        withCredentials([
                string(credentialsId: 'git.user.name', variable: 'GIT_USER'),
                string(credentialsId: 'git.user.email', variable: 'GIT_EMAIL')]) {
            sh "git config user.name '${GIT_USER}'"
            sh "git config user.email '${GIT_EMAIL}'"
        }
    }

    stage('Git Push') {
         withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "${githubCredentials}",
                            usernameVariable: 'USERNAME', passwordVariable: 'TOKEN']]) {
            sh "git push https://${TOKEN}:x-oauth-basic@github.com/${projectName}.git --tags"
        }
    }

}
